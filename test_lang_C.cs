/* -*- C -*- */

#include <stdio.h>
#include "test_lang.h"
#include "stddefer.h"

#pragma CMOD amend eval EXP0 EXP1
#pragma CMOD amend bind CASES=5

#define test_lang_C_ ## ${CASES} ## (...) test_lang_CPP_0(__VA_ARGS__)

#pragma EXP0 amend foreach CASE:N =             \
  exit(EXIT_SUCCESS)                            \
  exit(EXIT_FAILURE)                            \
  panic(-DEFER_ENOMEM)                          \
  panic(-DEFER_ERANGE,thrd_exit)                \
  return\ INT_MAX

#pragma EXP1 amend let M = ${N}+1
int test_lang_C_ ## ${N}(int rec) {
  int ret = 0;
  guard {
    printf("%s: rec %d, entering, test '" #${CASE} "'\n", __func__, rec);
    defer {
      printf("%s: rec %d, leaving, test '" #${CASE} "'\n", __func__, rec);
    }
    if (rec) {
      ret = test_lang_C_ ## ${M}(rec-1);
    } else {
      printf("%s calling the test\n", __func__);
      ${CASE};
    }
  }
  return ret;
}
#pragma EXP1 done

#pragma EXP0 done
#pragma CMOD done
#pragma CMOD done

