.SUFFIXES : .c .mk .o .h .s .hs .cs

CSOURCES =					\
	defer4.c				\
	defer3.c				\
	defer2.c				\
	defer.c					\
	stddefer.c				\
	test_lang.c				\
	test_lang_C.c

CPPSOURCES =					\
	test_lang++.cc				\
	stddefer_cpp.cc				\
	test_lang_CPP.cc

OBJECTS = $(CSOURCES:.c=.o) $(CPPSOURCES:.cc=.o)

MFILES = $(CSOURCES:.c=.mk) $(CPPSOURCES:.cc=.mk)

SFILES = $(CSOURCES:.c=.s) $(CPPSOURCES:.cc=.s)

SHNELL = ${HOME}/build/shnell/bin/shnell

COPT ?= -Wall -O3 -march=native

CFLAGS += -fPIC -ffunction-sections -fdata-sections ${COPT}
LDFLAGS += -Wl,-gc-sections

CXXFLAGS += ${CFLAGS}

ifeq (${DEFER_NO_WRAP},1)
CFLAGS += -DDEFER_NO_WRAP=1
else
LDWRAP += -Wl,-wrap=exit -Wl,-wrap=quick_exit -Wl,-wrap=thrd_exit
endif

ifeq (${DEFER_MALLOC_WRAP},1)
CFLAGS += -DDEFER_MALLOC_WRAP=1
LDWRAP += -Wl,-wrap=malloc -Wl,-wrap=calloc -Wl,-wrap=realloc -Wl,-wrap=aligned_alloc
endif

LDFLAGS += ${LDWRAP} -Wl,-rpath,. -L. -ldefer -pthread

target : makefiles libdefer.a libdefer.so defer defer2 defer3 defer4 test_lang test_lang++

% : %.o
	${CC}  ${CFLAGS} $^ ${LDFLAGS} -o $@

defer : defer.o

defer2 :  defer2.o

defer3 :  defer3.o

defer4 :  defer4.o

test_lang : test_lang.o test_lang_C.o test_lang_CPP.o
	${CXX}  ${CXXFLAGS} $^ ${LDFLAGS} -o $@

test_lang++ : test_lang++.o test_lang_C.o test_lang_CPP.o
	${CXX}  ${CXXFLAGS} $^ ${LDFLAGS} -o $@

libdefer.so : stddefer.o stddefer_cpp.o
	${CXX} -shared -o $@ $^ ${LDWRAP}

libdefer.a : libdefer.a(stddefer.o stddefer_cpp.o)

makefiles : ${MFILES}

%.mk : %.c
	echo -n "$*.s " > $@
	${CC} -M $< >> $@

%.s : %.c
	${CC} ${CFLAGS} -S $<

%.mk : %.cc
	echo -n "$*.s " > $@
	${CXX} -M $< >> $@

%.s : %.cc
	${CXX} ${CXXFLAGS} -S $<

%.h : %.hs
	WARN=" " ${SHNELL} $< | sed '/^#line/d ; 1,/endif/d' > $@

%.c : %.cs
	WARN=" " ${SHNELL} $< | sed '/^#line/d ; 1,/endif/d' > $@

%.cc : %.ccs
	WARN=" " ${SHNELL} $< | sed '/^#line/d ; 1,/endif/d' > $@

clean :
	rm -rf ${OBJECTS} ${MFILES} ${SFILES} libdefer.a libdefer.so defer defer2 defer3 defer4 defer-tests

-include ${MFILES}
