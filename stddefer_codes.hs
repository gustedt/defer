/* -*- C -*-  */

/*
 * The error codes for stddefer.h
 */

/*
 * BEWARE: the real source file is the file ending in .hs, it is
 * processed by shnell to produce the .h file. So don't modify the .h
 * file.
 */

#ifndef _STDDEFER_CODES_H
#define _STDDEFER_CODES_H 1

#ifdef __cplusplus
#include <cerrno>
#include <climits>
#include <csignal>
#include <atomic>
using std::atomic_int;
using std::atomic_short;
using std::sig_atomic_t;
#else
#include <errno.h>
#include <limits.h>
#include <signal.h>
#include <stdatomic.h>
// We still need this for static_assert
#include <assert.h>
#endif

// The unwind state shares a number range with signal numbers such
// that we can have them in the same member. This requires some
// acrobacy such that we do not have conflicts for the values.
#if ATOMIC_INT_LOCK_FREE > 1

typedef atomic_int _Defer_int;
enum { _DEFER_MAX = INT_MAX, };

#elif ATOMIC_SHORT_LOCK_FREE > 1

typedef atomic_short _Defer_int;
enum { _DEFER_MAX = SHRT_MAX, };

#else

typedef sig_atomic_t _Defer_int;
enum { _DEFER_MAX = SIG_ATOMIC_MAX, };

#endif


// Then we will also return `errno` numbers and signal numbers in the
// same integer, so here also we have to be sure not to map different
// events to the same number.
#define DEFER_SIGNO(X) (_DEFER_MAX-((int)+(X)))

#pragma CMOD amend eval EXP

/* Lists of errno and signal numbers. All of these are checked with
   `#ifdef` later on, so it should be ok to add a few more here that
   might appear on some platforms. */

#pragma CMOD amend bind                                                 \
  ELIST=                                                                \
  E2BIG EACCES EADDRINUSE EADDRNOTAVAIL EAFNOSUPPORT EAGAIN EALREADY    \
  EBADE EBADF EBADFD EBADMSG EBADR EBADRQC EBADSLT EBUSY ECANCELED ECHILD \
  ECHRNG ECOMM ECONNABORTED ECONNREFUSED ECONNRESET EDEADLK EDEADLOCK   \
  EDESTADDRREQ EDOM EDQUOT EEXIST EFAULT EFBIG EHOSTDOWN EHOSTUNREACH   \
  EHWPOISON EIDRM EILSEQ EINPROGRESS EINTR EINVAL EIO EISCONN EISDIR    \
  EISNAM EKEYEXPIRED EKEYREJECTED EKEYREVOKED EL2HLT EL2NSYNC EL3HLT    \
  EL3RST ELIBACC ELIBBAD ELIBEXEC ELIBMAX ELIBSCN ELNRANGE ELOOP        \
  EMEDIUMTYPE EMFILE EMLINK EMSGSIZE EMULTIHOP ENAMETOOLONG ENETDOWN    \
  ENETRESET ENETUNREACH ENFILE ENOANO ENOBUFS ENODATA ENODEV ENOENT     \
  ENOEXEC ENOKEY ENOLCK ENOLINK ENOMEDIUM ENOMEM ENOMSG ENONET ENOPKG   \
  ENOPROTOOPT ENOSPC ENOSR ENOSTR ENOSYS ENOTBLK ENOTCONN ENOTDIR       \
  ENOTEMPTY ENOTRECOVERABLE ENOTSOCK ENOTSUP ENOTTY ENOTUNIQ ENXIO      \
  EOPNOTSUPP EOVERFLOW EOWNERDEAD EPERM EPFNOSUPPORT EPIPE EPROTO       \
  EPROTONOSUPPORT EPROTOTYPE ERANGE EREMCHG EREMOTE EREMOTEIO ERESTART  \
  ERFKILL EROFS ESHUTDOWN ESOCKTNOSUPPORT ESPIPE ESRCH ESTALE ESTRPIPE  \
  ETIME ETIMEDOUT ETOOMANYREFS ETXTBSY EUCLEAN EUNATCH EUSERS           \
  EWOULDBLOCK EXDEV EXFULL                                              \
  EUNDERFLOW                                                            \
                                                                        \
  SLIST=                                                                \
  ABRT ALRM BUS CHLD CLD CONT EMT FPE HUP ILL INFO INT IO IOT KILL      \
  LOST PIPE POLL PROF PWR QUIT SEGV STKFLT STOP SYS TERM TRAP           \
  TSTP TTIN TTOU UNUSED URG USR1 USR2 VTALRM WINCH XCPU XFSZ            \
                                                                        \
  FLIST=                                                                \
  NONE EXIT FUNC BLCK

enum _Defer_codes {
      _Defer_Lev = _DEFER_MAX/2 - 1,
      /** This is supposed to be larger than any signal number. **/
      /** An inner guarded block **/
#pragma EXP amend foreach S:I = ${FLIST}
      _DEFER_##${S} = _Defer_Lev - ${I},
#pragma EXP done

      /** Signal numbers **/
#pragma EXP amend foreach S = ${SLIST}
#ifdef SIG##${S}
      DEFER_##${S} = DEFER_SIGNO(SIG##${S}),
#endif
#pragma EXP done

      _DEFER_EMAX = _Defer_Lev/2,

      /** system error numbers **/
#pragma EXP amend foreach S:I = ${ELIST}

#ifdef ${S}
      DEFER_##${S} = ${S},
#else
      DEFER_##${S} = _DEFER_EMAX - ${I},
#endif

#pragma EXP done

};

typedef enum _Defer_codes _Defer_codes;


// errno numbers should be small
#pragma EXP amend foreach S = ${ELIST}
static_assert(DEFER_##${S} < _DEFER_EMAX, "ranges for defer state values may overlap for " #${S});
#pragma EXP done

// signal numbers should be small
#pragma EXP amend foreach S = ${SLIST}
#ifdef SIG##${S}
static_assert(SIG##${S} < _DEFER_EMAX, "ranges for defer state values may overlap for SIG" #${S});
static_assert(_DEFER_NONE < DEFER_##${S}, "ranges for defer state values may overlap for SIG" #${S});
#endif
#pragma EXP done

#pragma EXP amend foreach S = ${FLIST}
static_assert(_DEFER_EMAX < _DEFER_##${S}, "ranges for defer state values may overlap for DEFER_" #${S});
#pragma EXP done

#pragma CMOD done
#pragma CMOD done

enum { _Defer_language_C, _Defer_language_Cpp, };

#ifdef __cplusplus
namespace std {
extern "C" int _Defer_language(int);
extern "C" int _Defer_cpp_get();
extern "C" void _Defer_panic_cpp(int, void (*)(int) = nullptr);
extern "C" void defer_exit(int);
extern "C" void defer_thrd_exit(int);
}
#endif

#ifdef __cplusplus
struct _Defer_language_Cpp_trigger {
  _Defer_language_Cpp_trigger() {
    std::_Defer_language(_Defer_language_Cpp);
  }
};
// Overloading main is a hack, but should work for C++ since it can't
// be called recursively.
#define main(...)                                                       \
  /* eat the leading int */                                             \
  _Defer_dummy_for_main;                                                \
  /* Define the object */                                               \
  static _Defer_language_Cpp_trigger _Defer_language_Cpp_trigger_object; \
  /* Define the real main */                                            \
  int main(__VA_ARGS__)
#endif


#endif
