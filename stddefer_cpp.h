#include "stddefer_codes.h"

#ifdef __cplusplus
#undef _Noreturn
#define _Noreturn [[noreturn]]
extern "C" {
#endif

  /**
   ** @brief Throw a C++ exception that translates error code @a err
   **
   ** For the moment this is relatively rudimentary and more a proof
   ** of concept than anything else. On the longer run detailed work
   ** should be invested to see which error numbers can be translated
   ** to which C++ exception type.
   **
   ** If nothing reasonable is found a generic object of type
   ** `panic_exception` is thrown.
   **/
  _Noreturn void _Defer_throw(int err, int act);

#ifdef __cplusplus
}
#endif
