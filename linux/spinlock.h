/**
 ** @file
 **
 ** This file just creates dummy functions to mockup kernal spinlock functionality.
 **/

#define SPIN_LOCK_UNLOCKED 0
#define SPIN_LOCK_LOCKED 1
typedef volatile int spinlock_t;

void spin_lock(spinlock_t *lock) {
    puts("spin_lock called");
    while (*lock == SPIN_LOCK_LOCKED);
    *lock = SPIN_LOCK_LOCKED;
}

void spin_unlock(spinlock_t *lock) {
    puts("spin_unlock called");
    *lock = SPIN_LOCK_UNLOCKED;
}
