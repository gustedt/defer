# defer reference implementation

This is a reference implementation of a `defer` feature for `C`. This is
a tool that is designed with golang's `defer/panic/recover` in mind,
adapting it to the specificities of `C`.
It aims to provide four properties to the handling of
circumstantial cleanup or error handling code:

**proximity -- visibility -- stability -- robustness**

The intent is to integrate the basic tools that are provided here into
the `C` standard by providing a new library clause for a new header with
the proposed name `<stddefer.h`.

The documentation can be found here:

[https://gustedt.gitlabpages.inria.fr/defer/](https://gustedt.gitlabpages.inria.fr/defer/)
