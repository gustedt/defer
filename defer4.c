#include <stdio.h>
#ifndef __STDC_NO_THREADS__
#include <threads.h>
#endif

#include <stddef.h>
#include <threads.h>
/**
 ** @file
 **
 ** This is a sample program for the defer mechanism that
 ** demonstrates the use of the panic mechanism.
 **/

// This should now always come after all system includes
#include "stddefer.h"

void g(int i) {
  if (i > 3) {
    puts("Panicking!");
    panic(i);
  }
  guard {
    defer {
      printf("Defer in g = %d.\n", i);
    }
    printf("Printing in g = %d.\n", i);
    g(i+1);
    // there a bug here and then end of the guard block has to
    // extend beyond the recursive function call for this to work properly
  }

}

void f() {
  guard {
    defer {
        puts("In defer in f");
        fflush(stdout);
      int err = recover();
      if (err != 0) {
        printf("Recovered in f = %d\n", err);
        fflush(stdout);
      }
    }
    puts("Calling g.");
    g(0);
    puts("Returned normally from g.");
  }
}

int main(int argc, char* argv[static argc+1]) {
  f();
  puts("Returned normally from f.");
  return EXIT_SUCCESS;
}
