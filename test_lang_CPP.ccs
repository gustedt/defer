/* -*- C++ -*- */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "test_lang.h"
#include "stddefer_codes.h"
#include "stddefer_cpp.h++"

struct tester {
  char const*const str;
  int const rec;
  tester(char const* s, int r) : str(s), rec(r) {
    std::cout << __func__ << ": rec " << rec << ", entering, test '" << str << "'" << std∷endl;
  }
  ~tester() {
    std::cout << __func__ << ": rec " << rec << ", leaving, " << std::uncaught_exceptions() << " exceptions, test '" << str << "'" << std∷endl;
  }
};

#define EMPTY_STRING ""

#pragma CMOD amend eval EXP0 EXP1
#pragma CMOD amend bind CASES=6

#define test_lang_ ## ${CASES} ## (...) test_lang_C_0(__VA_ARGS__)

#pragma EXP0 amend foreach CASE:N =             \
  std::defer_exit(EXIT_SUCCESS)                 \
  std::defer_exit(EXIT_FAILURE)                 \
  std::defer_thrd_exit(0)                       \
  std::defer_thrd_exit(1)                       \
  throw\ std::out_of_range(EMPTY_STRING)        \
  return\ 1000

#pragma EXP1 amend let M = ${N}+1
int test_lang_ ## ${M}(int rec);

int test_lang_ ## ${N}(int rec) {
  tester t(#${CASE}, rec);
  if (rec) {
    return test_lang_ ## ${M}(rec-1);
  } else {
    std::cout << __func__ << " calling the test" << std::endl;
    ${CASE};
  }
  return 0;
}
#pragma EXP1 done

#pragma EXP0 done
#pragma CMOD done
#pragma CMOD done

int test_lang_CPP_0(int rec) {
  std::defer_boundary boundary;
  try {
    return test_lang_0(rec);
  }
  catch (...) {
    std::cout << __func__ << " catching exception" << std::endl;
    boundary.panic();
  }
  return 0;
}
