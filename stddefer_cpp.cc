#include <ios>
#include <iostream>
#include "stddefer_cpp.h"
#include "stddefer_cpp.h++"

std::defer_exception::defer_exception(int err) : std::exception(), buf(), code(err) {
  if (err < 0) {
    if (err == INT_MIN) {
      snprintf(buf, buflen, "unknown panic in C runtime or library");
    } else {
      snprintf(buf, buflen, "panic in C runtime or library: %d", -err);
    }
  } else if (err > 0) {
    snprintf(buf, buflen, "application panic: %d", err);
  } else {
    snprintf(buf, buflen, "regular exit from C");
  }
}

// A auxiliary type to link exception types and C error codes

template< int code > struct _Defer_type {
  typedef int type;
};

#define TYPE_CASE(T, CODE)                                              \
template<> struct _Defer_type< CODE > { typedef T type; }

TYPE_CASE(std::out_of_range, DEFER_ERANGE);
TYPE_CASE(std::domain_error, DEFER_EDOM);
TYPE_CASE(std::ios_base::failure, DEFER_EBADF);
TYPE_CASE(std::bad_alloc, DEFER_ENOMEM);
TYPE_CASE(std::invalid_argument, DEFER_EINVAL);
TYPE_CASE(std::overflow_error, DEFER_EOVERFLOW);
TYPE_CASE(std::underflow_error, DEFER_EUNDERFLOW);


// Use that to translate from codes to exceptions

#define THROW_CASE(CODE)                                        \
  case -CODE:                                                   \
  std::cout << "translating panic at C-to-C++ boundary, code " #CODE << std::endl;           \
  throw _Defer_type< CODE >::type("translated C panic " #CODE)

extern "C"
[[noreturn]] void _Defer_throw(int err, int act) {

  if (act == _DEFER_EXIT) {
    std::cout << "translating exit or alike at C-to-C++ boundary" << std::endl;
    throw std::defer_exception(0);
  }

  // Otherwise generate a new exception according to our error code
  switch (err) {

    THROW_CASE(DEFER_ERANGE);
    THROW_CASE(DEFER_EDOM);
    THROW_CASE(DEFER_EBADF);
    THROW_CASE(DEFER_EINVAL);
    THROW_CASE(DEFER_EOVERFLOW);
    THROW_CASE(DEFER_EUNDERFLOW);
  case -DEFER_ENOMEM:
    std::cout << "translating panic at C-to-C++ boundary, code DEFER_ENOMEM" << std::endl;
    throw std::bad_alloc();

  default:
    std::cout << "translating unknown code " << err << " at C-to-C++ boundary" << std::endl;
    throw std::defer_exception(err);
  }
}


// And then translate back from exceptions to error codes

extern "C" void _Defer_panic_cpp(int, void (*)(int) = nullptr);


#define CATCH_CASE(CODE)                                        \
  catch (_Defer_type< CODE >::type&) {                          \
    std::cout << "translating to " #CODE << std::endl;          \
    return -CODE;                                               \
  }

int std::_Defer_cpp_get() {
  try {
    throw;
  }
  CATCH_CASE(DEFER_ERANGE)
    CATCH_CASE(DEFER_EDOM)
    CATCH_CASE(DEFER_EBADF)
    CATCH_CASE(DEFER_ENOMEM)
    CATCH_CASE(DEFER_EINVAL)
    CATCH_CASE(DEFER_EOVERFLOW)
    CATCH_CASE(DEFER_EUNDERFLOW)
    catch (std::defer_exception& err) {
      if (err.code) {
        std::cout << "caught C panic " << err.code << std::endl;
      } else {
        std::cout << "caught C exit or similar" << std::endl;
      }
      return err.code;
    }
    catch (...) {
      std::cout << "unknown exception " << std::endl;
    }
  return INT_MIN;
}


void std::defer_boundary::panic() {
  if (prev == _Defer_language_C) {
    std::cout << "translating exception at C++-to-C boundary, remaining exceptions " << std::uncaught_exceptions() << std::endl;
    _Defer_panic_cpp(_Defer_cpp_get());
  }
  throw;
}
