/* -*- C++ -*- */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "test_lang.h"
#include "stddefer_codes.h"
#include "stddefer_cpp.h++"

struct tester {
    char const*const str;
    int const rec;
    tester(char const* s, int r) : str(s), rec(r) {
        std::cout << __func__ << ": rec " << rec << ", entering, test '" << str << "'" << std::endl;
    }
    ~tester() {
        std::cout << __func__ << ": rec " << rec << ", leaving, " << std::uncaught_exceptions() << " exceptions, test '" << str << "'" << std::endl;
    }
};

#define EMPTY_STRING ""


#define test_lang_6(...) test_lang_C_0(__VA_ARGS__)


int test_lang_1(int rec);

int test_lang_0(int rec) {
    tester t("std::defer_exit(EXIT_SUCCESS)", rec);
    if (rec) {
        return test_lang_1(rec-1);
    } else {
        std::cout << __func__ << " calling the test" << std::endl;
        std::defer_exit(EXIT_SUCCESS);
    }
    return 0;
}


int test_lang_2(int rec);

int test_lang_1(int rec) {
    tester t("std::defer_exit(EXIT_FAILURE)", rec);
    if (rec) {
        return test_lang_2(rec-1);
    } else {
        std::cout << __func__ << " calling the test" << std::endl;
        std::defer_exit(EXIT_FAILURE);
    }
    return 0;
}


int test_lang_3(int rec);

int test_lang_2(int rec) {
    tester t("std::defer_thrd_exit(0)", rec);
    if (rec) {
        return test_lang_3(rec-1);
    } else {
        std::cout << __func__ << " calling the test" << std::endl;
        std::defer_thrd_exit(0);
    }
    return 0;
}


int test_lang_4(int rec);

int test_lang_3(int rec) {
    tester t("std::defer_thrd_exit(1)", rec);
    if (rec) {
        return test_lang_4(rec-1);
    } else {
        std::cout << __func__ << " calling the test" << std::endl;
        std::defer_thrd_exit(1);
    }
    return 0;
}


int test_lang_5(int rec);

int test_lang_4(int rec) {
    tester t("throw std::out_of_range(EMPTY_STRING)", rec);
    if (rec) {
        return test_lang_5(rec-1);
    } else {
        std::cout << __func__ << " calling the test" << std::endl;
        throw std::out_of_range(EMPTY_STRING);
    }
    return 0;
}


int test_lang_6(int rec);

int test_lang_5(int rec) {
    tester t("return 1000", rec);
    if (rec) {
        return test_lang_6(rec-1);
    } else {
        std::cout << __func__ << " calling the test" << std::endl;
        return 1000;
    }
    return 0;
}


int test_lang_CPP_0(int rec) {
    std::defer_boundary boundary;
    try {
        return test_lang_0(rec);
    }
    catch (...) {
        std::cout << __func__ << " catching exception" << std::endl;
        boundary.panic();
    }
    return 0;
}
